<?php
/**
 * Application level Controller 
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */

class AppController extends Controller {

	public function beforeFilter(){
		$authUser = AuthComponent::user();
		if($authUser){
			$this->layout = 'default';
			$user = $this->Session->read('Auth.User');
			$this->set('username', $user['name']);
			$this->set('avatar', $user['image']);
			
			$this->loadModel('Analysi');
			$analysi = $this->Analysi->find('first', array('recursive'=>-1, 'conditions'=>array('Analysi.user_id'=>$user['id'])));
			$this->set('analysi', $analysi);
		}

		else{
			$this->layout = 'default_notlogged';
		}
	}

	public $helpers = array('DateTime');

	public $components = array(
		'DebugKit.Toolbar',
		'Session',
		'Auth' => array(
            'loginAction' => array('controller' => 'users', 'action' => 'login'),
            'loginRedirect' => array('controller' => 'users', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
            'authError' => 'Você precisa efetuar login para acessar esta página!',
            'authorize' => array('Controller')
        )
	);

	public $uses = array('Analysi', 'Activity');

	/*
	 * Seta o titulo nas views do Admin
	 */
	function setLayoutTitle($title = null, $subtitle = null) {
		$this->set('layoutMenuActive', $this -> _setLayoutMenuActive($title,$subtitle));
		$this->set('layoutTitle', array('title' => $title, 'subtitle' => $subtitle));
		$this->set('title_for_layout', $title);
	}

	/*
	 * Seta o menu ativo no menu do Admin
	 */
	private function _setLayoutMenuActive($menu,$sub) {
		$return = array(
			'usuarios'=>null,
			'cursos' => null,
			'modalidades' => null,
			'tipos'=>null,
			'analises'=>null,
		);
		$section = strtolower(Inflector::slug(str_replace(' ', '', $menu), '-'));
		$return[$section] = 'active';

		return $return;
	}

	public function loadXml($file, $id){

		App::uses('Xml', 'Utility');

		$array = Xml::toArray(Xml::build($file));

		$curso = array();
		$atividades = array();
		foreach ($array as $key => $value) {
			$curso['name'] = $value['@curso'];
			foreach ($value['atividade'] as $key => $at) {
				$atividades[$key]['analysi_id'] = $id;
				$atividades[$key]['modality_id'] = $at['modalidade'];
				$atividades[$key]['type_id'] = $at['tipo'];
				$atividades[$key]['name'] = $at['nome'];
				$atividades[$key]['hours'] = $at['horas'];
			}
		}

		foreach ($atividades as $key => $value) {
			$this->Activity->saveAll($value);
		}
	}

	public function reloadXml($new, $id){

		App::uses('Xml', 'Utility');

		$array = Xml::toArray(Xml::build($new));

		$atividades = array();
		foreach ($array as $key => $value) {
			$curso['name'] = $value['@curso'];
			$curso['totalHours'] = 10;
			foreach ($value['atividade'] as $key => $at) {
				$atividades[$key]['analysi_id'] = $id;
				$atividades[$key]['modality_id'] = $at['modalidade'];
				$atividades[$key]['type_id'] = $at['tipo'];
				$atividades[$key]['name'] = $at['nome'];
				$atividades[$key]['hours'] = $at['horas'];
			}
		}

		$this->Activity->deleteAll(array('Activity.analysi_id' => $id), false);

		foreach ($atividades as $key => $value) {
			$this->Activity->saveAll($value);
		}
	}
}
