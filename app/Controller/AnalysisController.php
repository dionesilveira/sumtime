<?php
App::uses('AppController', 'Controller');

class AnalysisController extends AppController {

	public $components = array('Paginator', 'Upload');

	public function isAuthorized() {
		switch ($this->action) {
			case 'delete' :
			case 'select_status' :
			case 'viewPdf' :
			case 'pdfCoord' :
			case 'index' :
				if($this->Auth->User('level') >= 3) {
				    return true;
				}else{
					return false;
				}
			case 'add' :
			case 'edit' :
	       		return true;
	       		break;
	  	}
 	}

	public function index($course_id = null, $status_id = null) {

		if ($course_id == null && $status_id == null) {
			$filterStatus = null; 
			$filter = null;
			$conditions = array();
		}else if($course_id != 0 && $status_id != 0){
			$filter = $course_id;
			$filterStatus = $status_id;
			$conditions = array('Analysi.course_id'=>$course_id, 'Analysi.status'=>$status_id);
  		} else if($course_id == 0 && $status_id != null){
  			$filter = null;
			$filterStatus = $status_id;
			$conditions = array('Analysi.status'=>$status_id);
  		} else if($course_id != 0 && $status_id == null){
  			$filter = $course_id;
			$filterStatus = null;
			$conditions = array('Analysi.course_id'=>$course_id);
  		}

		$arguments = array('order' => array(
								'Analysi.created'=> 'ASC'
							),
							'recursive'=>0,
							'conditions' => $conditions
		);
		$courses = $this->Analysi->Course->find('list');
		$this->paginate = $arguments;
		$this->set('analysis', $this->Paginator->paginate());
		$this->set('filter', $filter);
		$this->set('filterStatus', $status_id);
		$this->set('courses', $courses);
		$this->set('status',Configure::read('STATUS'));
	}

	public function add() {

		if ($this->request->is('post')) {
			if (!empty($this->request->data['Analysi']['file'])) {
				$this->request->data['Analysi']['file'] = $this->Upload->sendArchive($this->request->data['Analysi']['file']);
				if ($this->request->data['Analysi']['file'] == false) {
					$this->Session->setFlash('Formato do arquivo inválido.', 'flash_error');
					return $this->redirect(array('action' => 'add'));
				}
			}
			$this->request->data['Analysi']['user_id'] = $this->Auth->user('id');
			$this->request->data['Analysi']['course_id'] = $this->Auth->user('course_id');
			$this->request->data['Analysi']['user_name'] = $this->Auth->user('name');
			$this->request->data['Analysi']['status'] = 1;

			$this->_isOwned($this->request->data);

			$this->Analysi->create();
			if ($this->Analysi->save($this->request->data)) {
				$id = $this->Analysi->id;
				$this->loadXml($this->request->data['Analysi']['file'], $id);

				$this->Session->setFlash('A análise foi salva corretamente.', 'flash_success');
				return $this->redirect(array('controller' => 'activities', 'action' => 'show', $id));
			} else {
				$this->Session->setFlash('A análise não pode ser salva. Por favor, tente novamente.');
			}
		}
	}

	public function edit($id = null) {
		$this->_isYour($id);
		if (!$this->Analysi->exists($id)) {
			throw new NotFoundException('Análise Inválida.', 'flash_error');
		}
		if ($this->request->is(array('post', 'put'))) {

			$xml_old = $this->Analysi->find('first', array('conditions'=>array('Analysi.id'=>$this->request->data['Analysi']['id'])));
			if (!empty($this->request->data['Analysi']['file_new'])) {
				$this->request->data['Analysi']['file'] = $this->Upload->sendArchive($this->request->data['Analysi']['file_new']);
				$this->Upload->removeFile($xml_old['Analysi']['file']);
			}

			if ($this->Analysi->save($this->request->data)) {

				$id = $this->Analysi->id;
				$this->reloadXml($this->request->data['Analysi']['file'], $id);

				$this->Session->setFlash('A análise foi alterada corretamente.', 'flash_success');
				return $this->redirect(array('controller' => 'activities', 'action' => 'show', $id));
			} else {
				$this->Session->setFlash('A análise não pode ser salva. Por favor, tente novamente.', 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Analysi.' . $this->Analysi->primaryKey => $id));
			$this->request->data = $this->Analysi->find('first', $options);
		}
	}

	public function delete($id = null) {
		$this->Analysi->id = $id;
		if (!$this->Analysi->exists()) {
			throw new NotFoundException('Registro inválido.', 'flash_error');
		}

		$file = $this->Analysi->find('first', array('conditions'=>array('Analysi.id'=>$id)));
		$this->Upload->removeFile($file['Analysi']['file']);

		$this->request->allowMethod('post', 'delete');

		if ($this->Analysi->Activity->deleteAll(array('Activity.analysi_id' => $id), false)
			&& $this->Analysi->Message->deleteAll(array('Message.analysi_id' => $id), false)) {
			$this->Analysi->delete();
			$this->Session->setFlash('A análise foi removida com sucesso', 'flash_success');
		} else {
			$this->Session->setFlash('A análise não pode ser removida. Por favor, tente novamente.', 'flash_error');
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function select_status($id = null, $status = null) {

		$this->request->data['Analysi']['id'] = $id;
		$this->request->data['Analysi']['status'] = $status;

		if ($this->Analysi->save($this->request->data)) {
			$this->Session->setFlash('Status alterado corretamente.', 'flash_success');
			return $this->redirect(array('controller' => 'activities', 'action' => 'show', $id));
		} else {
			$this->Session->setFlash('Status não pode ser alterado. Por favor, tente novamente.', 'flash_error');
		}
		$options = array('conditions' => array('Analysi.' . $this->Analysi->primaryKey => $id));
		$this->request->data = $this->Analysi->find('first', $options);
	}

	private function _isOwned($analysi) {
		$test = $this->Analysi->find('first', array('conditions' => array('Analysi.user_id'=>$analysi['Analysi']['user_id'])));
		if(!empty($test)){
			$this->Session->setFlash('Você já possui uma análise.', 'flash_warning');
			$this->redirect(array('controller' => 'activities', 'action' => 'show', $test['Analysi']['id']));
			return false;
		}else{
			return true;
		}
	}

	private function _isYour($id = null){
		$analysi = $this->Analysi->find('first', array('recursive'=>-1, 'conditions'=>array('Analysi.id'=>$id)));
		$userAnalysi = $this->Analysi->find('first', array('recursive'=>-1, 'conditions' => array('Analysi.user_id'=>$this->Session->read('Auth.User.id'))));

		if(sizeof($analysi) != 0){
			if($this->Session->read('Auth.User.id') != $analysi['Analysi']['user_id']){
				$this->Session->setFlash('Acesso negado! Seu usuário foi registrado tentando violar as regras do site, arque com as consequencias!','flash_error');
				$this->redirect(array('controller' => 'activities', 'action'=>'show', $userAnalysi['Analysi']['id']));
				return false;
			}else{
				return true;
			}
		}else{
			$this->Session->setFlash('Acesso negado! Seu usuário foi registrado tentando violar as regras do site, arque com as consequencias!','flash_error');
			$this->redirect(array('controller' => 'activities', 'action'=>'show', $userAnalysi['Analysi']['id']));
			return false;
		}
	}

	public function viewPdf($id) {
	    $this->layout = 'pdf';
		$analysi = $this->Analysi->find('first', array('conditions'=>array('Analysi.id'=>$id)));
		$this->set('analysi',$analysi);
	    $this->render();
	}

	public function pdfCoord(){
		$this->layout = 'pdf';

		if ($this->request->is(array('post', 'put'))){

			$final = array();
			$total = null;

			foreach ($this->request->data['Analysi'] as $key => $value) {
				if(isset($value['registration'])){
					$final[$key]['name'] = $value['name'];
					$total ++;
				}
			}
			if(sizeof($total) == 0){
				$this->Session->setFlash('Nenhum aluno foi selecionado.','flash_error');
				$this->redirect(array('controller' => 'analysis', 'action'=>'index'));
				return false;
			}else{
				$this->set('final',$final);
			}
		}

		$this->render();
	}

}
