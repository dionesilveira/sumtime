<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Users Controller
 *
 * @property User $User 
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

	public $components = array('Paginator', 'Upload');

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('add', 'conclusion', 'logout');
	}

	public function isAuthorized() {
		switch ($this->action) {
			case 'admin_add' :
			case 'admin_edit' :
			case 'delete' :
			case 'view' :
			case 'index' :
				if ($this->Auth->User('level') == 5) {
		     		return true;
		    		 break;
		       } else {
		     		return false;
		   			break;
		   	   }
			case 'add':
		   	case 'login':
			case 'logout':
			case 'edit' :
			case 'manageRedirect':
	       		return true;
	       		break;
	  	}
 	}
	
	public function manageRedirect(){		
		if ($this->Auth->User('level') == 5) {
			$this->redirect(array('action' => 'index'));
		} else if($this->Auth->User('level') == 4 || $this->Auth->User('level') == 3){
			$this->redirect(array('controller' => 'analysis', 'action'=>'index'));
		} else if($this->Auth->User('level') == 2){
			$userAnalysi = $this->User->Analysi->find('first', array('recursive'=>-1, 'conditions' => array('Analysi.user_id'=>$this->Session->read('Auth.User.id'))));
			$this->redirect(array('controller' => 'activities', 'action'=>'show', $userAnalysi['Analysi']['id']));
		}		
	}
	

	public function index() {

		$arguments = array('order'=>'User.name ASC', 'limit' => Configure::read('PAGINATE_LIMIT_ADMIN'), 'recursive'=>-1);
		$this -> paginate = $arguments;
		$this->set('users', $this->Paginator->paginate());
		$courses = $this->User->Course->find('list');
		$this->set('courses', $courses);
		$this->set('levels',Configure::read('LEVELS'));
		$this->layout="admin";
		$this->setLayoutTitle('Usuários','Lista');
	}

	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException('Usuário Inválido.', 'flash_error');
		}

		$arguments = array('conditions'=>array('User.id'=>$id), 'order'=>'User.name ASC', 'limit' => Configure::read('PAGINATE_LIMIT_ADMIN'), 'recursive'=>-1);
		$this -> paginate = $arguments;
		$this->set('users', $this->Paginator->paginate());
		$courses = $this->User->Course->find('list');
		$this->set('courses', $courses);
		$this->set('levels',Configure::read('LEVELS'));
		$this->layout="admin";
		$this->setLayoutTitle('Usuários','Visualizar');
	}


	public function admin_add() {
		if ($this->request->is('post')) {

			if (!empty($this->request->data['User']['image']['name'])) {
				$this->request->data['User']['image'] = $this->Upload->sendImageFixed($this->request->data['User']['image']);
				if ($this->request->data['User']['image'] == false) {
					$this->Session->setFlash('Formato do arquivo inválido.', 'flash_error');
					return $this->redirect(array('action' => 'conclusion', $id));
				}
			} else {
				$this->request->data['User']['image'] = null;
			}

			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('O usuário foi cadastrado corretamente.', 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('O usuário não pode ser cadastrado. Por favor, tente novamente.', 'flash_error');
			}
		}

		$courses = $this->User->Course->find('list');
		$this->set('course', $courses);
		$this->set('levels',Configure::read('LEVELS'));
		$this->layout="admin";
		$this->setLayoutTitle('Usuários','Adicionar');
	}

	public function admin_edit($id = null) {

		if (!$this->User->exists($id)) {
			throw new NotFoundException('Usuário Inválido.', 'flash_error');
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			if($this->request->data['User']['password_new'] != ''){

				if(!empty($this->request->data['User']['image_new']['name'])) {
					$this->request->data['User']['image'] = $this->Upload->sendImageFixed($this->request->data['User']['image_new']);
					if ($this->request->data['User']['image'] == false) {
						$this->Session->setFlash('Formato do arquivo inválido', 'flash_error');
						return $this->redirect(array('action' => 'admin_edit', $id));
					} else {
						$last_image = $this->User->find('first', array('conditions'=>array('User.id'=>$this->request->data['User']['id'])));
						$this->Upload->removeFile($last_image['User']['image']);
					}
				}

				$UserPass = $this->User->findById($this->request->data['User']['id']);
				if ($UserPass['User']['password'] == AuthComponent::password($this->request->data['User']['password_old'])){
					$this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password_new']);
					if ($this->User->save($this->request->data)) {
						$this->Session->setFlash('Dados atualizados corretamente.', 'flash_success');
						return $this->redirect(array('action' => 'index'));
					} else {
						$this->Session->setFlash('Erro ao salvar. Por favor, tente novamente.', 'flash_error');
					}
				} else {
					$this->Session->setFlash('Senha atual não confere.', 'flash_error');
				}
			}else{

				if(!empty($this->request->data['User']['image_new']['name'])) {
					$this->request->data['User']['image'] = $this->Upload->sendImageFixed($this->request->data['User']['image_new']);
					if ($this->request->data['User']['image'] == false) {
						$this->Session->setFlash('Formato do arquivo inválido', 'flash_error');
						return $this->redirect(array('action' => 'admin_edit', $id));
					} else {
						$last_image = $this->User->find('first', array('conditions'=>array('User.id'=>$this->request->data['User']['id'])));
						$this->Upload->removeFile($last_image['User']['image']);
					}
				}

				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash('Dados atualizados corretamente.', 'flash_success');
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash('Dados não atualizados. Por favor, tente novamente.', 'flash_error');
				}
			}

		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}

		$this->set('levels',Configure::read('LEVELS'));
		$courses = $this->User->Course->find('list');
		$this->set('course', $courses);
		$this->layout="admin";
		$this->setLayoutTitle('Usuários','Editar');
	}

	/*
	 * Funçao resposavel pelo pré-cadastro do aluno, enviando email de confirmação
	*/
	public function add() {
		if ($this->request->is('post')) {

			$this->_isRegistered($this->request->data);

			$this->request->data['User']['start'] = 1;

			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$id = $this->User->id;

				$email = new CakeEmail('default');
				$email -> template('user_add');
				$email -> to(array($this->request->data['User']['username']));
				$email -> subject('Cofirmação de email.');
				$email -> viewVars(array('obj' => array($this->request->data, $id)));
				$email -> send();

				$this->Session->setFlash('Email de confirmação enviado corretamente, verifique sua caixa de entrada.', 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Email de confirmação não enviado. Por favor, tente novamente.','flash_error');
			}
		}
	}

	/*
	 * Acesso somente pelos alunos para edição de seu perfil
	*/
	public function edit($id = null) {
		$this->_isYour($id);
		if (!$this->User->exists($id)) {
			throw new NotFoundException('Usuário Inválido.', 'flash_error');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			
			if($this->Session->read('Auth.User.level') == 2){
				$this->request->data['User']['level'] = 2;
			}

			if($this->request->data['User']['password_new'] != ''){

				if(!empty($this->request->data['User']['image_new']['name'])) {
					$this->request->data['User']['image'] = $this->Upload->sendImageFixed($this->request->data['User']['image_new']);
					if ($this->request->data['User']['image'] == false) {
						$this->Session->setFlash('Formato do arquivo inválido.', 'flash_error');
						return $this->redirect(array('action' => 'edit', $id));
					} else {
						$last_image = $this->User->find('first', array('conditions'=>array('User.id'=>$this->request->data['User']['id'])));
						$this->Upload->removeFile($last_image['User']['image']);
					}
				}

				$UserPass = $this->User->findById($this->request->data['User']['id']);
				if ($UserPass['User']['password'] == AuthComponent::password($this->request->data['User']['password_old'])){
					$this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password_new']);
					if ($this->User->save($this->request->data)) {
						$this->Session->setFlash('Dados atualizados corretamente.', 'flash_success');
						$analysi = $this->User->find('first', array('conditions'=>array('User.id'=>$id)));
						if($this->Session->read('Auth.User.level') == 2){
							return $this->redirect(array('controller' => 'activities', 'action' => 'show', $analysi['Analysi']['id']));	
						}else if($this->Session->read('Auth.User.level') == 3 || $this->Session->read('Auth.User.level') == 4){
							return $this->redirect(array('controller' => 'analysis', 'action' => 'index'));	
						}else{
							return $this->redirect(array('controller' => 'users', 'action' => 'index'));	
						}
						
					} else {
						$this->Session->setFlash('Erro ao salvar. Tente novamente.', 'flash_error');
					}
				} else {
					$this->Session->setFlash('Senha atual não confere.', 'flash_error');
				}
			}else{

				if(!empty($this->request->data['User']['image_new']['name'])) {
					$this->request->data['User']['image'] = $this->Upload->sendImageFixed($this->request->data['User']['image_new']);
					if ($this->request->data['User']['image'] == false) {
						$this->Session->setFlash('Formato do arquivo inválido.', 'flash_error');
						return $this->redirect(array('action' => 'edit', $id));
					} else {
						$last_image = $this->User->find('first', array('conditions'=>array('User.id'=>$this->request->data['User']['id'])));
						$this->Upload->removeFile($last_image['User']['image']);
					}
				}

				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash('Dados atualizados corretamente.', 'flash_success');
					$analysi = $this->User->find('first', array('conditions'=>array('User.id'=>$id)));
					if($this->Session->read('Auth.User.level') == 2){
							return $this->redirect(array('controller' => 'activities', 'action' => 'show', $analysi['Analysi']['id']));	
						}else if($this->Session->read('Auth.User.level') == 3 || $this->Session->read('Auth.User.level') == 4){
							return $this->redirect(array('controller' => 'analysis', 'action' => 'index'));	
						}else{
							return $this->redirect(array('controller' => 'users', 'action' => 'index'));	
						}
				} else {
					$this->Session->setFlash('Dados não atualizados. Por favor, tente novamente.', 'flash_error');
				}
			}

		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}

		$courses = $this->User->Course->find('list');
		$this->set('course', $courses);
	}

	/*
	 * Funçao responsavel pela conclusao de cadastro do aluno
	 * */
	public function conclusion($id = null) {
		$this->_isOwned($id);

		if ($this->request->is(array('post', 'put'))) {
			if (!empty($this->request->data['User']['image']['name'])) {
				$this->request->data['User']['image'] = $this->Upload->sendImageFixed($this->request->data['User']['image']);
				if ($this->request->data['User']['image'] == false) {
					$this->Session->setFlash('Formato do arquivo inválido.', 'flash_error');
					return $this->redirect(array('action' => 'conclusion', $id));
				}
			} else {
				$this->request->data['User']['image'] = null;
			}

			$this->request->data['User']['level'] = 2;
			$this->request->data['User']['start'] = 0;
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('Cadastro efetuado realizado com sucesso.', 'flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Cadastro não efetuado. Por favor, tente novamente.', 'flash_error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$courses = $this->User->Course->find('list');
		$this->set('course', $courses);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException('Usuário Inválido!', 'flash_error');
		}

		$analysi = $this->User->Analysi->find('first', array('recursive'=>-1, 'conditions'=>array('Analysi.user_id'=>$id)));

		$this->request->onlyAllow('post', 'delete');
		if(sizeof($analysi) != 0){
			if ($this->User->Analysi->Activity->deleteAll(array('Activity.analysi_id' => $analysi['Analysi']['id']), false)
				&& $this->User->Message->deleteAll(array('Message.user_id'=>$id, 'Message.analysi_id' => $analysi['Analysi']['id']), false)
				&& $this->User->Analysi->deleteAll(array('Analysi.user_id' => $id), false)) {
				$this->User->Delete();
				$this->Session->setFlash('Usuário removido com sucesso.','flash_success');
			} else {
				$this->Session->setFlash('O usuário não pode ser removido. Por favor, tente novamente.', 'flash_error');
			}
		}else{
			if($this->User->delete()){
				$this->Session->setFlash('Usuário removido com sucesso.','flash_success');
			}else{
				$this->Session->setFlash('O usuário não pode ser removido. Por favor, tente novamente.', 'flash_error');
			}
		}

		return $this->redirect(array('action' => 'index'));
	}

	public function login(){
		if ($this->request->is('post')) {
            if ($this->Auth->login()) {
            	if($this->Auth->user('level')  < 3){
					if($this->Auth->user('Analysi.id') == null){
	        			$this->redirect(array('controller' => 'analysis', 'action' => 'add'));
	        		}else{
	        			$this->redirect(array('controller' => 'activities', 'action' => 'show', $this->Auth->user('Analysi.id')));
	        		}
				}else if($this->Auth->user('level') == 3 || $this->Auth->user('level') == 4){
					$this->redirect(array('controller' => 'analysis', 'action' => 'index'));
				}else if($this->Auth->user('level') == 5){
					$this->redirect(array('controller' => 'users', 'action' => 'index'));
				}
            } else {
                $this->Session->setFlash('Login inválido!','flash_error');
            }
        }
	}

	public function logout(){
		$this->redirect($this->Auth->logout());
	}

	/*
	 * Funçao que verifica o acesso à conclusao de cadastro
	*/
	private function _isOwned($user) {
		$test = $this->User->find('first', array('conditions' => array('User.id'=>$user)));
		if(!$this->User->exists($user) || $test['User']['start'] == 0){
			$this->Session->setFlash('Para ter acesso à essa página, realize o cadastro.', 'flash_warning');
			$this->redirect(array('controller' => 'users', 'action' => 'login'));
			return false;
		}else{
			return true;
		}
	}

	/*
	 * Ferifica se o email informado no pré-cadastro já esta cadastrado no sistema
	 */
	private function _isRegistered($now) {
		$users = $this->User->find('all', array('recursive' => -1));

		foreach ($users as $user) {
			if($user['User']['username'] == $now['User']['username']){
				$this->Session->setFlash('Esse email já possui cadastro.', 'flash_error');
				$this->redirect(array('controller' => 'users', 'action' => 'login'));
				return false;
			}
		}
		return true;
	}

	/*
	 * Funçao responsavel por verificar se o aluno esta editando seu proprio perfil
	*/
	private function _isYour($id) {
		$user = $this->User->find('first', array('conditions' => array('User.id'=>$id)));
		$userAnalysi = $this->User->Analysi->find('first', array('recursive'=>-1, 'conditions' => array('Analysi.user_id'=>$this->Session->read('Auth.User.id'))));

		if($this->Session->read('Auth.User.level') < 5){
			if($this->Session->read('Auth.User.id') != $user['User']['id']){
				if($this->Session->read('Auth.User.level') == 2){
					$this->Session->setFlash('Acesso negado! Seu usuário foi registrado tentando violar as regras do site, arque com as consequencias!','flash_error');
					$this->redirect(array('controller' => 'activities', 'action'=>'show', $userAnalysi['Analysi']['id']));
					return false;
				}else{
					$this->Session->setFlash('Acesso negado! Seu usuário foi registrado tentando violar as regras do site, arque com as consequencias!', 'flash_error');
					$this->redirect(array('controller' => 'analysis', 'action'=>'index'));
					return false;
				}
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
}
