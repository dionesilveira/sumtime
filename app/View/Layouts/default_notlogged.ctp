<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo 'sumTime' ?>
	</title>
	<?php
		echo $this -> Html -> meta('icon');//
		echo $this -> Html -> css('base');
		echo $this -> Html -> css('header');
		echo $this -> Html -> css('footer');
		echo $this -> Html -> css('bootstrap.min');//
		echo $this -> Html -> css('bootstrap-theme.min');//
		echo $this -> Html -> css('prettyPhoto');//
		echo $this -> Html -> script('jquery.min');
		echo $this -> Html -> script('bootstrap');
		echo $this -> Html -> script('bootstrap.min');
		echo $this -> Html -> script('jquery-1.3.2.min');
		echo $this -> Html -> script('jquery-1.4.4.min');
		echo $this -> Html -> script('jquery-1.6.1.min');
		echo $this -> Html -> script('header'); 
		echo $this -> Html -> script('jquery.prettyPhoto');
		echo $this -> Html -> script('geral');
		echo $this -> Html -> script('validate'); 
	
		echo $this -> fetch('meta');
		echo $this -> fetch('css');
		echo $this -> fetch('script');
	?>
	
</head>
<body>
<div class="sumtime_Container">
	<div class="sumtime_Header">
		<div class="logo_container">
			<img src="<?php echo $this->webroot; ?>images/sumtime.png" style="height:35px !important; max-height:35px; margin-right:-10px;"/>
		</div>

		<div class="header_UserArea_notLogged">
			<a href="<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'login'));?>">Login</a>    <span style="color:white;">|</span>   <a href="<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'add'));?>">Registrar</a>
		</div>		
	</div>
	<div id="container">
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
	
	<div class="sumtime_Footer">
		<center>
			<br>
			
			<div class="logos">
				<a href="https://www.ufpel.edu.br"><img src="<?php echo $this->webroot; ?>images/logoufpel.png" style="height:120px !important; max-height:120px; margin-right:-10px;"/> </a>
				<a href="http://inf.ufpel.edu.br"><img src="<?php echo $this->webroot; ?>images/computacao.png" /> </a>
				<a href="http://cdtec.ufpel.edu.br/"><img src="<?php echo $this->webroot; ?>images/cdteclogo.png" style="height:50px" /> </a>
			</div>
			Computação - UFPel | (53) 3921 1327 | Campus Porto - UFPel, Rua Gomes Carneiro 1, 96010-610 | Pelotas-RS

		</center>
	</div>
</div>
</body>

</html>
