<div class="activities index">
	<?php
		echo $this->Html->script('jquery-1.3.2.min');
		echo $this->Html->script('hours_sum');
		echo $this->Html->script('bootstrap-dropdown');
		echo $this -> Html -> script('bootstrap');
		echo $this->Html->script('jquery.prettyPhoto');
		echo $this->Html->css('prettyPhoto');
	?>
	
	<legend><?php echo 'Atividades de '?><?php echo $analysi['User']['name'];?></legend>
	<table cellpadding="0" cellspacing="0" class="table table-hover">
	<tr>
			<th><?php echo 'Nome'; ?></th>
			<th><?php echo 'Tipo'; ?></th>
			<th><?php echo 'Modalidade'; ?></th>
			<th><?php echo 'Horas'; ?></th>
			<th><?php echo 'Certificado'; ?></th>
			<th><?php echo 'Ações' ?></th>
	</tr>
	
	<?php foreach ($activities as $activity): ?>
	<tr>
		
		<td><?php echo h($activity['Activity']['name']); ?>&nbsp;</td>
		<td>
			<?php echo h($activity['Type']['name']); ?>
		</td>
		<td>
			<?php echo h($activity['Modality']['name']); ?>
		</td>
		<td><?php echo h($activity['Activity']['hours']); ?>&nbsp;</td>
		<?php if ($activity['Activity']['document']):?>  
			 <td><span class="btn btn-info btn-small" style="height:25px; padding:3px; padding-top:0px;"><p style="text-align: left;"><strong><a title="Certificado de <?php echo$activity['Activity']['name'];?>" style="font-weight:normal; color:white; padding:5px;" href="<?php echo "/sumtime/app/webroot/{$activity['Activity']['document']}"?>?iframe=true&amp;width=80%&amp;height=100%" rel="prettyPhoto">Visualizar</a></strong></p></span></td>
		<?php else: ?> 
			<td><span class="btn btn-info btn-small" style="height:25px; padding:3px; padding-top:0px;"><a style="font-weight:normal; color:white; padding:5px;" href="<?php echo $this->Html->url(array('controller'=>'activities', 'action'=>'addCertificate', $activity['Activity']['id']));?>">Add Certificado</a></span></td>
		<?php endif ?>
		<td class="actions">
			<span class="btn btn-info btn-small" style="height:25px; padding:3px; padding-top:0px;"><?php echo $this->Html->link(('Editar'), array('action' => 'edit', $activity['Activity']['id'])); ?></span>
			<span class="btn btn-info btn-small" style="height:25px; padding:3px; padding-top:0px;"><?php echo $this->Form->postLink(('Remover'), array('action' => 'delete', $activity['Activity']['id']), array(), __('Você tem certeza que deseja remover # %s?', $activity['Activity']['id'])); ?></span>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	
		<legend><?php echo ('Comentários'); ?></legend>		
		<div class="container" style="width:100%;">
				<div class="row">
					<div class="panel panel-default widget">

						<div class="panel-heading">
							<h3 class="panel-title">
								Comentários Recentes</h3>
							<span class="label label-info comments_counter">
								<?php echo sizeof($messages) ?></span>
						</div>
						
				<div class="panel-body">
					<ul class="list-group">
						<?php foreach ($messages as $message): ?>
							<li class="list-group-item">
								<div class="row">
									<div class="col-xs-2 col-md-1">	
										<?php if ($message['User']['image']): ?>
											<?php echo $this->Html->image('../'.$message['User']['image'], array('style'=>'width: 50px; height: 50px; border:1px solid gray;')); ?>
										<?php else: ?>
											<img src="http://placehold.it/80" class="img-circle img-responsive" alt="" />
										<?php endif; ?>
										</div>
									<div class="col-xs-10 col-md-11">
										<div>
											Autor: <b><?php echo $message['User']['name']; ?></b>
											<div class="mic-info">
												Enviado em: <?php echo $this->DateTime->formatDateTime($message['Message']['created']);?>
											</div>
										</div>
										<div class="comment-text" style="white-space:pre-line;">
											<?php echo $message['Message']['description']; ?>
										</div>
										<div class="action" style="text-align:right;">
											<?php if($this->Session->read('Auth.User.id') == $message['Message']['user_id']) :?>
											<?php echo $this->Form->postLink(__('Remover'), array('controller' => 'messages','action' => 'delete', $message['Message']['id']), null, __('Você tem certeza que deseja remover o comentário # %s?', $activity['Activity']['id'])); ?>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</li>
						<?php endforeach ?>
					</div>
					<div style="text-align:center;">
						<br>
						<?php
							echo $this->Form->create('Message', array('action' => 'add'));
							echo $this->Form->input('analysi_id', array('value' => $analysi['Analysi']['id'], 'type' => 'hidden'));
							echo $this->Form->input('user_id', array('type' => 'hidden'));
							echo $this->Form->input('description', array('label' => 'Enviar Comentário'));
						?>
						<?php
							echo $this->Form->submit(
								'Enviar', 
								array('class' => 'btn btn-info', 'style' => 'width:90px; text-align:center;','title' => 'Enviar')
							);	
						?>
					</div>
				</div>
			</div>
		</div>
</div>

<div class="actions">
	<ul>
		<?php if ($this->Session->read('Auth.User.level') < 3): ?>
			<li class="btn btn-info btn-block voltar" style="padding:5px;padding-top:3px;"><?php echo $this->Html->link(('Alterar Análise'), array('controller' => 'analysis', 'action' => 'edit', $analysi['Analysi']['id'])); ?></li>
		<?php else: ?> 
			<li class="btn btn-info btn-block voltar" style="padding:5px;padding-top:3px;"><?php echo $this->Html->link(('Retornar à Análises'), array('controller' => 'analysis', 'action' => 'index')); ?></li>
		<?php endif ?>
	</ul>
	<br/>
	<?php if($this->Session->read('Auth.User.level') == 3): ;?>	
		<h2 style="text-align:center;font-size:150% !important;"><?php echo 'Alterar Status '?></h2>
	
		<div  style="left:51px;" class="btn-group">
			<button data-toggle="dropdown" class="btn btn-info btn-small dropdown-toggle">
				<?php
					echo $statuses[$analysi['Analysi']['status']];
				?>
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu dropdown-info pull-right" style="position:relative;">
				<?php	foreach ($statuses as $key => $obj) {
						   echo '<li>';
						   echo $this->Html->link($obj, array('controller' => 'analysis', 'action' => 'select_status', $analysi['Analysi']['id'], $key));
						   echo '</li>';
						}
				?>
		 	</ul>
		</div>
		<br><br>
	<?php endif ?>
	<!-- HUEHUEHUEHUE -->
	
	<?php if($this->Session->read('Auth.User.level') < 3) ; ?>
		<h2 style="text-align:center;font-size:150% !important;"><?php echo 'Status da Análise'?></h2>
		
		<?php if ($statuses[$analysi['Analysi']['status']] == "Em Análise"): ;?>
			<h4 style="text-align:center;"><span class="label label-warning"><?php echo $statuses[$analysi['Analysi']['status']]; ?></span></h3>
		<?php endif; ?>
		<?php if ($statuses[$analysi['Analysi']['status']] == "Concluído"): ;?>
			<h4 style="text-align:center;"><span class="label label-success"><?php echo $statuses[$analysi['Analysi']['status']]; ?></span></h3>
		<?php endif; ?>
		
		<?php if ($statuses[$analysi['Analysi']['status']] == "Pendente"): ;?>
			<h4 style="text-align:center;"><span class="label label-danger"><?php echo $statuses[$analysi['Analysi']['status']]; ?></span></h3>		
	<?php endif; ?>
	</br>
	</br>
		<h5><b>Curso</b></h5>
		<?php echo $analysi['Course']['name'];?></br>
	<tr>
		<h5><b><?php echo 'Horas Ensino'?></b></h5>
		<span id="horas_ensino" style="color:black;"><?php echo $ensino ?></span> / <span id="min_ensino"><?php echo $modalities[0]['Modality']['hours']?></span>
	</tr>
	<tr>
		<h5><b><?php echo 'Horas Pesquisa'?></b></h5>
		<span id="horas_pesquisa"><?php echo $pesquisa ?></span> / <span id="min_pesquisa"><?php echo $modalities[1]['Modality']['hours']?></span>
	</tr>
	<tr>
		<h5><b><?php echo 'Horas Extensão'?></b></h5>
		<span id="horas_extensao"><?php echo $extensao ?></span> / <span id="min_extensao"><?php echo $modalities[2]['Modality']['hours']?></span>
	</tr>
	
	<?php if ($this->Session->read('Auth.User.level') > 2): ?>
	<tr>
		<p id="hours_advice" style="margin-top:20px;text-align:justify;color:red;">
			
		</p>

	</tr>
	<?php else: ?>
		<p id="hours_advice" style="display:none;">
			
		</p>
	<?php endif; ?>
</div>
