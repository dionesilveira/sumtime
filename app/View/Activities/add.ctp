<div class="activities form">
<?php echo $this->Form->create('Activity', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Add Activity'); ?></legend>
	<?php
		echo $this->Form->input('analysi_id');
		echo $this->Form->input('modality_id');
		echo $this->Form->input('type_id');
		echo $this->Form->input('name');
		echo $this->Form->input('document');
		echo $this->Form->input('hours');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Activities'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Analysis'), array('controller' => 'analysis', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Analysi'), array('controller' => 'analysis', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Modalities'), array('controller' => 'modalities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Modality'), array('controller' => 'modalities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'types', 'action' => 'add')); ?> </li>
	</ul>
</div>
