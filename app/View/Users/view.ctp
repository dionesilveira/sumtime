<table class="table table-striped table-bordered table-hover">
	<thead>
		<th>Imagem</th>
		<th>Nível</th>
		<th><?php echo $this->Paginator->sort('name', 'Nome'); ?></th>
		<th>Curso</th>
		<th>Usuário</th>
	</thead>
	<tbody>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo $this->Html->image('../'.$user['User']['image'], array('style'=>'width: 50px; height: 50px')); ?></td>
				<td><?php echo h($levels[$user['User']['level']]); ?></td>
				<td><?php echo h($user['User']['name']); ?></td>
				<?php if(isset($user['User']['course_id'])):?>
					<td><?php echo h($courses[$user['User']['course_id']]); ?></td>
				<?php else: ?>
					<td><?php echo 'realizando cadastro'; ?></td>
				<?php endif; ?>
				<td><?php echo h($user['User']['username']); ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>