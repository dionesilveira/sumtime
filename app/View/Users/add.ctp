<?php
	echo $this -> Html -> css('bootstrap.min');	
	echo $this -> Html -> css('add_user');
	echo $this -> Html -> script('validate'); 
?>

<p>Bem-vindo, novo usuário! O sumTime é um sistema feito para automatizar a contagem de horas extra-curriculares dos cursos de Computação da Universidade
Federal de Pelotas. Para fazer upload de sua análise, você deve baixar a versão <a href="#">Desktop</a> e montar o arquivo XML. Após
montando, deve ser importado em <strong>Nova Análise</strong>. Caso de dúvidas, entre em contato com o Coordenador de Atividades via
o sistema de comentários dentro da <strong>Análise</strong>. <br><br>
	Para cadastrar uma nova conta, você deve possuir um email do domínio @inf.ufpel.edu.br. Caso não possua um, entre em contato com a Secretaria
	do curso. Mais informacões em <a href="http://inf.ufpel.edu.br">inf.ufpel.edu.br</a>.
</p>

<div class="form-signin">
<?php echo $this->Form->create('User', array('novalidate' => true)); ?>
	<h2 class="form-signin-heading">Cadastrar</h2>
	<br>
	<?php
		echo $this->Form->input('name', array('label' =>'Nome', 'class' => 'form-control', 'placeholder'=>'Nome'));
		echo $this->Form->input('start', array('type'=>'hidden'));
		echo $this->Form->input('username', array('label' =>'Email', 'class' => 'form-control', 'placeholder'=>'@inf.ufpel.edu.br'));
		echo '<div class="form-actions"><input class="btn btn-lg btn-primary btn-block" id="btnSalvar" type="button" value="Confirmar"></div>';
	?>
	

<?php echo $this->Form->end(); ?>
</div>
	    <p style="text-align:center;"> <strong><span style="color:red; font-size:18px;">*</span>Informe seu email (@inf.ufpel.edu.br) e, após pressionar confirmar, 
	    siga os passos que receberá no email para finalizar o cadastro!</strong></p>

<script>
	$('#btnSalvar').click(function(){
		if (validateUser()){
			$('#UserAddForm').submit();
		}
	});
</script>

