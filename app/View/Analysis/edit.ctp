<div class="analysis form">
<?php echo $this->Form->create('Analysi', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo 'Alterar Análise'; ?></legend>
		<p>Nesta tela você pode alterar sua análise enviando um novo arquivo de atividades gerado pela versão Desktop. Note que suas atividades
		anteriores serão sobrescritas pelo XML novo, então certifique-se de que o arquivo esteja completo!</p>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id', array('type' => 'hidden'));
		echo $this->Form->input('file_new', array('type' => 'file', 'label' => 'Arquivo (.xml)'));
		echo $this->Form->input('status', array('type' => 'hidden'));
	?>
	</fieldset>
	<?php
		echo $this->Form->submit(
			'Salvar', 
			array('class' => 'btn btn-info btn-block', 'style' => 'width:75px;')
		);	
	?>
	<?php echo $this->Form->end(); ?>
	</form>
</div>

<div class="actions">
	<?php if($this->Session->read('Auth.User.level') == 2):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'activities', 'action'=>'show', $this->request->data['Analysi']['id']));?>'">Retornar à Minha Análise</button>
	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 3 || $this->Session->read('Auth.User.level') == 4):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'analysis', 'action'=>'index'));?>'">Retornar à Lista de Análises</button>
	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 5):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'index'));?>'">Retornar à Painel de Admin</a></li>
	<?php endif; ?>
</div>

