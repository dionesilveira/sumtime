<?php
	echo $this->Form->create('Modality', 
					array('class'=> 'form-horizontal', 'type' => 'file', 'inputDefaults' => array(
        				'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
        				'div' => array('class' => 'control-group'),
        				'label' => array('class' => 'control-label'),
        				'between' => '<div class="controls">',
        				'after' => '</div>',
        				'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')))));
	
	echo $this->Form->input('id');
	echo $this->Form->input('name', array('label' =>array('text' => 'Nome', 'class' => 'control-label')));
	echo $this->Form->input('hours', array('label' => array('text' => 'Horas totais', 'class' => 'control-label')));
	echo $this->Form->input('course_id', array('style' => 'width:220px;', 'empty'=>array(''=>'Selecione'), 'label' =>array('text' => 'Curso', 'class' => 'control-label')));
	
	echo '<div class="form-actions"><input class="btn btn-small btn-success" id="btnSalvar" type="button" value="Salvar"></div>';
	echo $this->Form->end();
?>

<script>
	$('#btnSalvar').click(function(){
		$('#ModalityEditForm').submit();
	});
</script>
