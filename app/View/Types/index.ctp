<?php echo $this -> Html -> link(__('Adicionar'), array('action' => 'add'), array('class'=>'btn btn-small btn-primary')); ?>
<br><br>
<?php if (sizeof($types)==0) : echo 'Nenhum resultado foi encontrado.'; endif ?>
	
<?php if (sizeof($types)>0) : ?>

<table class="table table-striped table-bordered table-hover">
	<thead>
		<th><?php echo $this->Paginator->sort('course_id', 'Curso'); ?></th>
		<th><?php echo 'Nome'?></th>
		<th><?php echo 'Horas/Semestre'?></th>
		<th><?php echo 'Modalidade'?></th>
		<th></th>
	</thead>
	<tbody>
		<?php foreach ($types as $type): ?>
			<tr>
				<td><?php echo h($courses[$type['Type']['course_id']]); ?></td>
				<td><?php echo h($type['Type']['name']); ?></td>
				<td><?php echo h($type['Type']['hours']); ?></td>
				<td><?php echo h($type['Modality']['name']); ?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('Editar'), array('action' => 'edit',$type['Type']['id'])); ?>
					<?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete',$type['Type']['id']), null, __('Deseja excluir o registro #%s?', $type['Type']['name'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php echo $this->Element('admin/Layout/paginate');?>

<?php endif; ?>