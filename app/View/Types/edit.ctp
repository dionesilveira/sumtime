<?php
	echo $this->Form->create('Type', 
					array('class'=> 'form-horizontal', 'type' => 'file', 'inputDefaults' => array(
        				'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
        				'div' => array('class' => 'control-group'),
        				'label' => array('class' => 'control-label'),
        				'between' => '<div class="controls">',
        				'after' => '</div>',
        				'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')))));
	
	echo $this->Form->input('id');
	echo $this->Form->input('name', array('label' =>array('text' => 'Nome', 'class' => 'control-label')));
	echo $this->Form->input('hours', array('label' =>array('text' => 'Horas/Semestre', 'class' => 'control-label')));
	echo $this->Form->input('course_id', array('style' => 'width:220px;', 'empty'=>array(0=>'Selecione'), 'label' =>array('text' => 'Curso', 'class' => 'control-label')));
	echo $this->Form->input('modality_id', array('value'=>$this->request->data['Type']['modality_id'], 'style' => 'width:220px;', 'label' =>array('text' => 'Modalidade', 'class' => 'control-label')));
	
	echo '<div class="form-actions"><input class="btn btn-small btn-success" id="btnSalvar" type="button" value="Salvar"></div>';
	echo $this->Form->end();
?>

<script>
	$('#btnSalvar').click(function(){
		$('#TypeEditForm').submit();
	});
	
	function getCourse() {	
		var course_id = $('#TypeCourseId').val();
		console.log(course_id);
		$('#TypeModalityId').html('');
		$.post('../../modalities/ajaxGetModalities', {
			'data[course_id]' : course_id,
			}, function(data) {
				if (data) {
					var toAppend = '';
					 toAppend += '<option value="">Selecione</options>';
					$.each(data,function(i,o){
						if(i == <?php echo $this->request->data['Type']['modality_id']?>){
							toAppend += '<option value='+i+' selected="selected">'+o+'</option>';
						}else{
							toAppend += '<option value='+i+'>'+o+'</option>';
						}
					});
					$('#TypeModalityId').append(toAppend);
				} else {
					alert('erro');
				}
		}, 'json');
	}
	$('#TypeCourseId').ready(function() {
		getCourse();
	});
	$('#TypeCourseId').click(function() {
		getCourse();
	});
</script>
